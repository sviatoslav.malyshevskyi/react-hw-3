import { ADD_ITEM, REMOVE_ITEM, DELETE_ITEM, CLEAR_CART, UPDATE_CART, MODAL_CLOSE, MODAL_OPEN } from './constants';

const ShoppingCartUpdate = (shoppingCart, item, index) => {
  console.log(item.count)
  if (item.count === 0) {
    return [...shoppingCart.slice(0, index), ...shoppingCart.slice(index + 1)];
  }

  if (index === -1) {
    return [...shoppingCart, item];
  }

  return [...shoppingCart.slice(0, index), item, ...shoppingCart.slice(index + 1)];
};

const ShoppingCartItemUpdate = (products, item = {}, quantity) => {
  const {
    id = products.id,
    title = products.title,
    image = products.image,
    price = products.price,
    count = 0,
    total = 0,
  } = item;
  return {
    id,
    title,
    image,
    price,
    count: count + quantity,
    total: Number((total + quantity * products.price).toFixed(2)),
  };
};

const ShoppingCartRefresh = (state, productId, quantity) => {
  const {products, shoppingCart} = state;
  const merchandise = products.find(({id}) => id === productId);
  const itemIndex = shoppingCart.findIndex(({id}) => id === productId);
  const item = shoppingCart[itemIndex];
  const newCartItem = ShoppingCartItemUpdate(merchandise, item, quantity);
  return ShoppingCartUpdate(shoppingCart, newCartItem, itemIndex);
}

const reducer = (state, {type, payload}) => {
  switch (type) {
    case ADD_ITEM: {
      const newShoppingCart = ShoppingCartRefresh(state, payload, 1);
      localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCart));
      return {...state, shoppingCart: newShoppingCart};
    }
    case REMOVE_ITEM: {
      const newShoppingCart = ShoppingCartRefresh(state, payload, -1);
      localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCart));
      return {...state, shoppingCart: newShoppingCart};
    }
    case DELETE_ITEM: {
      const item = state.shoppingCart.find(({id}) => id === payload);
      const newShoppingCart = ShoppingCartRefresh(state, payload, -item.count);
      localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCart));
      return {...state, shoppingCart: newShoppingCart};
    }
    case UPDATE_CART: {
      const item = state.shoppingCart.find(({id}) => id === payload.index);
      const newShoppingCart = ShoppingCartRefresh(state, payload.index, payload.value - item.count);
      localStorage.setItem('shoppingCart', JSON.stringify(newShoppingCart));
      return {...state, shoppingCart: newShoppingCart};
    }
    case CLEAR_CART:
      localStorage.setItem('shoppingCart', '');
      return {...state, shoppingCart: []};
    case MODAL_OPEN:
      return {...state, isModalOpen: true};
    case MODAL_CLOSE:
      return {...state, isModalOpen: false};
    default:
      return state;
  }
};

export default reducer;