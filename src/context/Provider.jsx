import React, { useReducer } from 'react';
import reducer from './reducer';
import context from './context';
import initialState from './initialState';
import { ADD_ITEM, REMOVE_ITEM, DELETE_ITEM, CLEAR_CART, UPDATE_CART, MODAL_OPEN, MODAL_CLOSE } from './constants';

const Provider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = {
    state,
    addItem: (index) => dispatch({type: ADD_ITEM, payload: index}),
    removeItem: (index) => dispatch({type: REMOVE_ITEM, payload: index}),
    deleteItem: (index) => dispatch({type: DELETE_ITEM, payload: index}),
    updateCart: (value, index) => dispatch({type: UPDATE_CART, payload: {value, index}}),
    clearCart: () => dispatch({type: CLEAR_CART}),
    modalOpen: () => dispatch({type: MODAL_OPEN}),
    modalClose: () => dispatch({type: MODAL_CLOSE}),
  };

  return (
      <context.Provider value={value}>
        {children}
      </context.Provider>
  );
};

export default Provider;
