import React, { useContext } from 'react';
import context from '../../../context/context';
import Button from 'react-bootstrap/Button';
import './Navbar.css';

const Navbar = () => {
    const {state: {shoppingCart}, modalOpen, clearCart} = useContext(context);
    const totalCount = shoppingCart.reduce((sum, item) => sum + item.count, 0);

    return (
        <nav className="navbar navbar-inverse bg-inverse fixed-top bg-faded">
            <div className="row nav-row">
                <div className="col">
                    <Button variant="primary" onClick={modalOpen}
                            data-toggle="modal" data-target="#cart">
                        Cart (<span className="total-count">{totalCount}</span>)
                    </Button>{' '}
                    <Button variant="primary" onClick={clearCart}>Clear Cart</Button>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
