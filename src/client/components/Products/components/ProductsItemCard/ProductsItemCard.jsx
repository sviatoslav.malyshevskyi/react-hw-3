import React, { useContext } from 'react';
import Col from "react-bootstrap/cjs/Col";
import context from '../../../../../context/context';
import Button from 'react-bootstrap/Button';
import './ProductsItemCard.css'

const ProductsItemCard = (product) => {
  const {addItem} = useContext(context);
  const {id, title, image, price} = product;
  return (
      <Col key={id} className="col">
        <div className="card">
          <img className="card-img-top" src={image} alt="Card img" />
          <div className="card-block">
            <h4 className="card-title">{title}</h4>
            <p className="card-text">Price: ${price}</p>
            <Button type="button" variant="primary" onClick={() => addItem(id)}>Add to Cart</Button>
          </div>
        </div>
      </Col>
  );
};

export default ProductsItemCard;
