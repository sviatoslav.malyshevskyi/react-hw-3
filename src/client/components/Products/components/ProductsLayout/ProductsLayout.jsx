import React, { useContext } from 'react';
import Container from "react-bootstrap/cjs/Container";
import Col from "react-bootstrap/cjs/Col";
import Row from "react-bootstrap/cjs/Row";
import context from '../../../../../context/context';
import './ProductsLayout.css';
import ProductsItemCard from '../ProductsItemCard';

const ProductsLayout = () => {
  const {products} = useContext(context).state;
  const productsItems = products.map(item => <ProductsItemCard key={item.id} {...item}/>)

  return (
      <>
        <Container className="pl3prc pt3prc">
          <Col>
            <h1>Products:</h1>
            <Row className="flex">{productsItems}</Row>
          </Col>
        </Container>
      </>
  );
};

export default ProductsLayout;
