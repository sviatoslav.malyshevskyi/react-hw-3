import React, { useContext } from 'react';
import context from '../../../context/context';
import Button from 'react-bootstrap/Button';
import './ModalContainer.css';

const ModalContainer = ({title, children}) => {
  const {state: {isModalOpen}, modalClose} = useContext(context);
  const verifyOpenState = isModalOpen ? 'modal modal-open' : 'modal';

    return (
      <div className={verifyOpenState}>
        <div className="modal-lg">
          <div className="modal-content">
            <div className="modal-header flex a-j-center-between pl10prc">
              <h5 className="modal-title">{title}</h5>
              <Button variant="primary" className="close btn-sm" onClick={modalClose}><span> x </span></Button>
            </div>
            <div className="modal-body">{children}</div>
            <div className="modal-footer">
              <Button variable="danger" onClick={modalClose} className="btn-right btn-danger">Close</Button>
            </div>
          </div>
        </div>
      </div>
    );
  };

export default ModalContainer;
