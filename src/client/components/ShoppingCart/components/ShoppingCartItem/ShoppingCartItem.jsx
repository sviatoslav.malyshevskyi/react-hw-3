import React, { useContext } from 'react';
import context from '../../../../../context/context';
import Button from 'react-bootstrap/Button';
import './ShoppingCartItem.css';

const ShoppingCartItem = (props) => {
  const {addItem, removeItem, deleteItem, updateCart} = useContext(context);
  const {id, title, image, price, count, total} = props;
  return (
      <tr>
        <th scope="row" className="flex a-j-center-between">
          <div className="flex-column">
           <div className="ital">
             {title}
           </div>
            <img src={image} width="80px" height="80px" alt="Cart item"/>
          </div>
        </th>
        <td className="pl-pr-5">${price}</td>
        <td>
          <div className="input-group-lg">
            <Button type="button" variable="primary" onClick={() => removeItem(id)}> - </Button>{' '}
            <input type="number" onChange={e => updateCart(e.target.value, id)}
                   className="form-control txt-c" title={title} value={count} style={{width: "30px"}}
            />{' '}
            <Button type="button" variable="primary" onClick={() => addItem(id)}> + </Button>
          </div>
        </td>
        <td className="txt-c pl-pr-5">${total}</td>
        <td className="pl10">
          <Button type="button" variable="danger" onClick={() => deleteItem(id)}> х </Button>
        </td>
      </tr>
  );
};

export default ShoppingCartItem;
