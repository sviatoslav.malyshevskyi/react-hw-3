import React, { useContext } from 'react';
import context from '../../../../../context/context';
import './ShoppingCartEmpty.css'

const ShoppingCartEmpty = () => {
  const {shoppingCart} = useContext(context).state;
  const displayMessage = (shoppingCart.length > 0) ? 'empty-cart' : 'empty-cart show'

  return (
      <h6 className={displayMessage}><span className="red txt-lg">Your cart is empty!</span></h6>
  );
};

export default ShoppingCartEmpty;
