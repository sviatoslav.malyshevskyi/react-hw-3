import React from 'react';
import 'react-bootstrap';
import './App.css';
import Provider from './context/Provider';
import Navbar from './client/components/Navbar';
import ProductsLayout from './client/components/Products/components/ProductsLayout';
import ModalContainer from './client/components/ModalContainer';
import ShoppingCartEmpty from './client/components/ShoppingCart/components/ShoppingCartEmpty';
import ShoppingCartPopulated from './client/components/ShoppingCart/components/ShoppingCartPopulated';

function App() {
  return (
      <Provider>
        <div className="App">
          <Navbar />
          <ProductsLayout />
          <ModalContainer title='Shopping Cart'>
            <ShoppingCartEmpty />
            <ShoppingCartPopulated />
          </ModalContainer>
        </div>
      </Provider>
  );
}

export default App;
